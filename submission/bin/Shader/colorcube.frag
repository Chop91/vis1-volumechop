#version 400 core

layout(location = 0) out vec4 o_color;

in vec4 f_color;

void main()
{
	o_color = f_color;
}