#version 400 core

layout(location = 0) out vec4 o_color;

in vec2 f_uv;

uniform sampler1D u_texture_transfer_function;
uniform sampler2D u_texture_front;
uniform sampler2D u_texture_back;
uniform sampler3D u_texture_volume;
uniform sampler3D u_texture_gradient;

uniform float u_unit_step;
uniform float u_sample_distance;

uniform int u_shading;
uniform float u_diffuse_shading;
uniform float u_specular_shading;
uniform float u_specular_exponent;
uniform float u_shading_intensity;
uniform float u_gradient_shading;
uniform float u_gradient_opacity;

uniform vec3 u_light_position;
uniform vec3 u_volume_position;

vec4 shade(vec4 color, vec3 normal, vec3 light_direction, vec3 half_vec, float magnitude)
{
	vec3 diffuse_rgb = color.rgb * max(0.0, dot(normal, light_direction));
	diffuse_rgb = mix(color.rgb, diffuse_rgb, u_diffuse_shading);
	vec3 specular_rgb = u_specular_shading * vec3(1.0, 1.0, 1.0) * pow(max(0.0, dot(normal, half_vec)), u_specular_exponent);
	vec3 shaded_rgb = mix(color.rgb, diffuse_rgb + specular_rgb, u_shading_intensity);

	float interpolation_factor = smoothstep(0.0, 1.0 - u_gradient_shading, magnitude);
	shaded_rgb = mix(color.rgb, shaded_rgb, interpolation_factor);

	interpolation_factor = smoothstep(0.0, 1.0 - u_gradient_opacity, magnitude);
	float shaded_a = interpolation_factor * color.a;

	return vec4(shaded_rgb, shaded_a);
}

void main()
{
	vec3 frontface = texture(u_texture_front, f_uv).xyz;
	vec3 backface = texture(u_texture_back, f_uv).xyz;
	vec3 view_direction = normalize(backface - frontface);

	float step_size = u_unit_step * u_sample_distance;
	vec3 ray_increment = view_direction * step_size;

	vec3 light_direction = normalize(u_light_position - u_volume_position);
	vec3 half_vec = (view_direction + light_direction) / length(view_direction + light_direction);
	
	o_color = vec4(0.0, 0.0, 0.0, 0.0);

	for(float ray_length = length(backface - frontface); ray_length > 0.0; ray_length = ray_length - step_size)
	{
		// Get the sample value (GL_LUMINANCE: r = g = b; a = 1.0)
		float sample_value = (texture(u_texture_volume, frontface)).g;

		// Get the color and opacity from the transfer function
		vec4 sample_color = (texture(u_texture_transfer_function, sample_value));

		if(sample_color.a > 0.0)
		{
			// Gradient-based shading if enabled
			if(u_shading == 1)
			{
				vec3 gradient = (texture(u_texture_gradient, frontface)).rgb;

				float magnitude = length(gradient);
				vec3 normal = normalize(gradient);

				sample_color = shade(sample_color, normal, light_direction, half_vec, magnitude);
			}

			// Performing Front-To-Back Alpha-Compositing using the Over-Operator
			float alpha = o_color.a + sample_color.a * (1.0 - o_color.a);
			vec3 rgb = (o_color.rgb * o_color.a + sample_color.rgb * sample_color.a * (1.0 - o_color.a)) / alpha;
			o_color = vec4(rgb, alpha);
		}

		// Calculate the next position
		frontface += ray_increment;

		// Early Ray Termination
		if(o_color.a >= 1.0)
		{
			break;
		}
	}
}