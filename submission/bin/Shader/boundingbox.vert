#version 400 core

layout(location = 1) in vec3 v_position;

uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * vec4(v_position, 1.0);
}