Group: VolumeChop

Group members:
Patrick Mayr	1226745	E033 534

Remarks:
Just execute 'VolumeChop.exe'.
Settings can be adjusted in 'VolumeChop.exe.config'.

Controls:
PRESS	SPACE	Hide/Unhide the 'Volume Rendering'- and the 'Transformation'-Bar
PRESS	B		Enable/Disable the rendering of the bounding box
HOLD	Z		Zoom with the mouse wheel
HOLD	S		Scale the object with the mouse wheel
PRESS	A		Enable/Disable the auto rotation of the object
HOLD	R		Rotate the object with the mouse
PRESS	UP		Increase rotation speed
PRESS	DOWN	Drcrease rotation speed