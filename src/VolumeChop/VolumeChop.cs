﻿using AntTweakBar;
using System;
using System.Diagnostics;
using VolumeChop.Renderer;

namespace VolumeChop
{
    class VolumeChop
    {
        // Catching exceptions according to: https://github.com/TomCrypto/AntTweakBar.NET/blob/master/Samples/OpenTK-OpenGL/Program.cs

        public const int ExitSuccess = 0;
        public const int ExitFailure = 1;

        [STAThread]
        public static int Main()
        {
            try
            {
                using (VolumeRenderer volumeRenderer = new VolumeRenderer())
                {
                    volumeRenderer.Run();
                }

                return ExitSuccess;
            }
            catch (AntTweakBarException e)
            {
                Trace.TraceError("AntTweakBar error: " + e.Message
                    + "Exception details: " + e.Details
                    + "\n======= STACK TRACE =======\n"
                    + e.StackTrace);

                return ExitFailure;
            }
            catch (Exception e)
            {
                Trace.TraceError("An error occurred: " + e.Message
                    + "\n======= STACK TRACE =======\n"
                    + e.StackTrace);

                return ExitFailure;
            }
        }
    }
}