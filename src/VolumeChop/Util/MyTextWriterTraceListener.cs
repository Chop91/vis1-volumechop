﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace VolumeChop.Util
{
    internal class MyTextWriterTraceListener : TextWriterTraceListener
    {

        public MyTextWriterTraceListener()
            : base(String.Format("{0}_{1:yyyy-MM-dd_HH-mm-ss}.log", ConfigurationManager.AppSettings["windowName"], DateTime.Now))
        {
            Filter = new EventTypeFilter(SourceLevels.Information);
        }

        // Create custom label
        public override void Write(string message)
        {
            List<string> messageParts = message.Split(' ').ToList();

            string label;
            string type = messageParts.ElementAtOrDefault(1);

            if (type == null)
            {
                label = String.Format("{0:yyyy-MM-dd HH:mm:ss}: ", DateTime.Now);
            }
            else
            {
                label = String.Format("{0:yyyy-MM-dd HH:mm:ss},{1} ", DateTime.Now, type);
            }
            Writer.Write(label);
        }
    }
}