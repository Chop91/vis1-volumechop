﻿using AntTweakBar;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;

namespace VolumeChop.Renderer
{
    class GUI
    {
        // Constants
        private const float MarginScale = 0.025f;

        private const float MinScale = 0.1f;
        private const float MaxScale = 2.0f;
        private const float ScaleStep = 0.01f;

        private const float MinFov = 30.0f;
        private const float MaxFov = 90.0f;
        private const float FovStep = 0.5f;


        private Context context;
        private Bar volumeRenderingBar;
        private Bar transformationBar;
        private BoolVariable autoRotation;
        private FloatVariable rotationSpeed;
        private FloatVariable scaleVar;
        private FloatVariable fovVar;
        private QuaternionVariable rotationVar;
        private BoolVariable boundingBoxVar;

        private bool scaleEnabled;
        private bool rotationEnabled;
        private bool zoomEnabled;


        private int orgX;
        private int orgY;
        private int rotX;
        private int rotY;
        private int preRotX;
        private int preRotY;

        private Size windowSize;
        private double renderPeriod;

        public Parameter Parameter { get; private set; }

        public GUI(Size windowSize, double renderPeriod)
        {
            this.windowSize = windowSize;
            this.renderPeriod = renderPeriod;
            Parameter = new Parameter();
            InitializeParameters(Parameter.DataSet.HeadSmall); // Default dataset

            context = new Context(Tw.GraphicsAPI.OpenGLCore);
            InitializeVolumeRenderingBar();
            InitializeTransformationBar();
        }

        public void InitializeParameters(Parameter.DataSet dataSet)
        {
            // Always store your parameter there
            string path = @"./Data/Parameter/";
            string filename = "";

            if (dataSet.Equals(Parameter.DataSet.Lobster))
            {
                filename = @"lobster_120x120x34.xml";
            }
            else if (dataSet.Equals(Parameter.DataSet.BeetleSmall))
            {
                filename = @"beetle_138x138x82.xml";
            }
            else if (dataSet.Equals(Parameter.DataSet.Beetle))
            {
                filename = @"beetle_277x277x164.xml";
            }
            else if (dataSet.Equals(Parameter.DataSet.HeadSmall))
            {
                filename = @"head_128x128x112.xml";
            }
            else if (dataSet.Equals(Parameter.DataSet.Head))
            {
                filename = @"head_256x256x225.xml";
            }

            var xml = XDocument.Load(Path.Combine(path, filename));

            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add("", Path.Combine(path, @"parameter.xsd"));

            xml.Validate(schemas, (o, e) =>
            {
                Trace.TraceError("{0}\n", e.Message);
                System.Environment.Exit(1);
            });


            #region queries

            // Volume queries
            var generalQuery = from g in xml.Root.Descendants("general")
                               select g.Element("data").Value + " "
                                    + g.Element("bounding_box").Value;

            var raycastingQuery = from r in xml.Root.Descendants("raycasting")
                                  select r.Element("sample_distance").Value + " "
                                       + r.Element("rendering_technique").Value;

            var shadingQuery = from s in xml.Root.Descendants("shading")
                               select s.Element("shading").Value + " "
                                    + s.Element("shading_intensity").Value + " "
                                    + s.Element("diffuse_shading").Value + " "
                                    + s.Element("specular_shading").Value + " "
                                    + s.Element("specular_exponent").Value + " "
                                    + s.Element("gradient_shading").Value + " "
                                    + s.Element("gradient_opacity").Value;

            var tfnQuery = from tfn in xml.Root.Descendants("transfer_function_node")
                           select tfn.Attribute("id").Value + " "
                                + tfn.Element("color").Element("r").Value + " "
                                + tfn.Element("color").Element("g").Value + " "
                                + tfn.Element("color").Element("b").Value + " "
                                + tfn.Element("color").Element("a").Value + " "
                                + tfn.Element("control_point").Value;

            var visualMappingQuery = from vm in xml.Root.Descendants("visual_mapping")
                                     select vm.Element("glyph_density").Value + " "
                                       + vm.Element("glyph_threshold").Value;


            // Transformation queries
            var cameraQuery = from c in xml.Root.Descendants("camera")
                              select c.Element("field_of_view").Value + " "
                                    + c.Element("camera_position").Element("vector").Element("x").Value + " "
                                    + c.Element("camera_position").Element("vector").Element("y").Value + " "
                                    + c.Element("camera_position").Element("vector").Element("z").Value + " "
                                    + c.Element("camera_direction").Element("vector").Element("x").Value + " "
                                    + c.Element("camera_direction").Element("vector").Element("y").Value + " "
                                    + c.Element("camera_direction").Element("vector").Element("z").Value;

            var lightQuery = from l in xml.Root.Descendants("light")
                             select l.Element("light_position").Element("vector").Element("x").Value + " "
                                    + l.Element("light_position").Element("vector").Element("y").Value + " "
                                    + l.Element("light_position").Element("vector").Element("z").Value;

            var objectQuery = from o in xml.Root.Descendants("object")
                              select o.Element("scale").Value + " "
                                     + o.Element("auto_rotation").Value + " "
                                     + o.Element("rotation_speed").Value + " "
                                     + o.Element("rotation").Element("quaternion").Element("x").Value + " "
                                     + o.Element("rotation").Element("quaternion").Element("y").Value + " "
                                     + o.Element("rotation").Element("quaternion").Element("z").Value + " "
                                     + o.Element("rotation").Element("quaternion").Element("w").Value + " "
                                     + o.Element("translation").Element("vector").Element("x").Value + " "
                                     + o.Element("translation").Element("vector").Element("y").Value + " "
                                     + o.Element("translation").Element("vector").Element("z").Value;

            #endregion


            string[] param;

            // Volume
            #region general

            param = generalQuery.First().Split(' ');

            // Load default dataset defined in the xml file on start
            if (context == null)
            {
                // Mind: These are the defined types in the xsd
                if (param[0].Equals("Lobster"))
                {
                    Parameter.Data = Parameter.DataSet.Lobster;
                }
                else if (param[0].Equals("BeetleSmall"))
                {
                    Parameter.Data = Parameter.DataSet.BeetleSmall;
                }
                else if (param[0].Equals("Beetle"))
                {
                    Parameter.Data = Parameter.DataSet.Beetle;
                }
                else if (param[0].Equals("HeadSmall"))
                {
                    Parameter.Data = Parameter.DataSet.HeadSmall;
                }
                else if (param[0].Equals("Head"))
                {
                    Parameter.Data = Parameter.DataSet.Head;
                }

            }
            Parameter.BoundingBox = Convert.ToBoolean(param[1]);

            #endregion

            #region raycasting

            param = raycastingQuery.First().Split(' ');

            Parameter.SampleDistance = Convert.ToSingle(param[0], CultureInfo.InvariantCulture.NumberFormat);

            // Mind: These are the defined types in the xsd
            if (param[1].Equals("MIP"))
            {
                Parameter.RenderingTechnique = Parameter.Technique.MIP;
            }
            else if (param[1].Equals("DVR"))
            {
                Parameter.RenderingTechnique = Parameter.Technique.DVR;
            }
            else if (param[1].Equals("Average"))
            {
                Parameter.RenderingTechnique = Parameter.Technique.Average;
            }
            else if (param[1].Equals("Gyph"))
            {
                Parameter.RenderingTechnique = Parameter.Technique.Glyph;
            }

            #endregion

            #region shading

            param = shadingQuery.First().Split(' ');

            Parameter.Shading = Convert.ToBoolean(param[0]);
            Parameter.ShadingIntensity = Convert.ToSingle(param[1], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.DiffuseShading = Convert.ToSingle(param[2], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.SpecularShading = Convert.ToSingle(param[3], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.SpecularExponent = Convert.ToSingle(param[4], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.GradientShading = Convert.ToSingle(param[5], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.GradientOpacity = Convert.ToSingle(param[6], CultureInfo.InvariantCulture.NumberFormat);

            #endregion

            #region transfer function

            // Transfer function
            Parameter.TransferFunctionNodes = new List<TransferFunctionNode>();
            foreach (string parameter in tfnQuery)
            {
                //string[] param = parameter.Split(' ');
                param = parameter.Split(' ');
                int id = Convert.ToInt32(param[0]);
                float r = Convert.ToSingle(param[1], CultureInfo.InvariantCulture.NumberFormat);
                float g = Convert.ToSingle(param[2], CultureInfo.InvariantCulture.NumberFormat);
                float b = Convert.ToSingle(param[3], CultureInfo.InvariantCulture.NumberFormat);
                float a = Convert.ToSingle(param[4], CultureInfo.InvariantCulture.NumberFormat);
                float controlPoint = Convert.ToSingle(param[5], CultureInfo.InvariantCulture.NumberFormat);

                Parameter.TransferFunctionNodes.Add(new TransferFunctionNode(new Color4(r, g, b, a), controlPoint, id));
            }

            #endregion

            #region visual mapping

            param = visualMappingQuery.First().Split(' ');

            Parameter.GlyphDensity = Convert.ToSingle(param[0], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.GlyphThreshold = Convert.ToSingle(param[1], CultureInfo.InvariantCulture.NumberFormat);

            #endregion


            // Transformation
            #region camera

            param = cameraQuery.First().Split(' ');

            Parameter.FieldOfView = Convert.ToSingle(param[0], CultureInfo.InvariantCulture.NumberFormat);

            float x = Convert.ToSingle(param[1], CultureInfo.InvariantCulture.NumberFormat);
            float y = Convert.ToSingle(param[2], CultureInfo.InvariantCulture.NumberFormat);
            float z = Convert.ToSingle(param[3], CultureInfo.InvariantCulture.NumberFormat);

            Parameter.CameraPosition = new Vector3(x, y, z);

            x = Convert.ToSingle(param[4], CultureInfo.InvariantCulture.NumberFormat);
            y = Convert.ToSingle(param[5], CultureInfo.InvariantCulture.NumberFormat);
            z = Convert.ToSingle(param[6], CultureInfo.InvariantCulture.NumberFormat);

            Parameter.CameraDirection = new Vector3(x, y, z);

            #endregion

            #region light

            param = lightQuery.First().Split(' ');

            x = Convert.ToSingle(param[0], CultureInfo.InvariantCulture.NumberFormat);
            y = Convert.ToSingle(param[1], CultureInfo.InvariantCulture.NumberFormat);
            z = Convert.ToSingle(param[2], CultureInfo.InvariantCulture.NumberFormat);

            Parameter.LightPosition = new Vector3(x, y, z);

            #endregion

            #region object

            param = objectQuery.First().Split(' ');

            Parameter.Scale = Convert.ToSingle(param[0], CultureInfo.InvariantCulture.NumberFormat);
            Parameter.AutoRotation = Convert.ToBoolean(param[1]);
            Parameter.RotationSpeed = Convert.ToSingle(param[2], CultureInfo.InvariantCulture.NumberFormat);

            x = Convert.ToSingle(param[3], CultureInfo.InvariantCulture.NumberFormat);
            y = Convert.ToSingle(param[4], CultureInfo.InvariantCulture.NumberFormat);
            z = Convert.ToSingle(param[5], CultureInfo.InvariantCulture.NumberFormat);
            float w = Convert.ToSingle(param[6], CultureInfo.InvariantCulture.NumberFormat);

            Parameter.Rotation = new Quaternion(x, y, z, w);
            Parameter.RotationMatrix = Matrix4.CreateFromQuaternion(Parameter.Rotation);

            x = Convert.ToSingle(param[7], CultureInfo.InvariantCulture.NumberFormat);
            y = Convert.ToSingle(param[8], CultureInfo.InvariantCulture.NumberFormat);
            z = Convert.ToSingle(param[9], CultureInfo.InvariantCulture.NumberFormat);

            Parameter.Translation = new Vector3(x, y, z);

            #endregion


            // Delete existing bars when loading a new model
            if (context != null)
            {
                context.Clear();
                InitializeVolumeRenderingBar();
                InitializeTransformationBar();
            }

        }

        private void InitializeVolumeRenderingBar()
        {
            volumeRenderingBar = new Bar(context)
            {
                Label = "Volume Rendering",
                Contained = true,
                ValueColumnWidth = 0,
                Iconified = false,
                Help = "Press SPACE to hide/unhide the 'Volume Rendering'-Bar.",
                Size = new Size(250, 550), // TODO: size and columnwidth
                Position = new Point((int)(MarginScale * windowSize.Width), (int)(MarginScale * windowSize.Height)) // TODO: position
            };
            volumeRenderingBar.SetDefinition("refresh=" + renderPeriod + " color='17 109 143' alpha=64");


            var generalGroup = new Group(volumeRenderingBar);
            var raycastingGroup = new Group(volumeRenderingBar);
            var shadingGroup = new Group(volumeRenderingBar);
            var transferfunctionGroup = new Group(volumeRenderingBar);
            var visualMappingGroup = new Group(volumeRenderingBar);


            // General
            var dataSetVar = new EnumVariable<Parameter.DataSet>(volumeRenderingBar, Parameter.Data);
            dataSetVar.Label = "Data Set";
            dataSetVar.Changed += delegate { Parameter.Data = dataSetVar.Value; };
            dataSetVar.Group = generalGroup;

            boundingBoxVar = new BoolVariable(volumeRenderingBar, Parameter.BoundingBox);
            boundingBoxVar.Label = "Bounding Box";
            boundingBoxVar.Help = "Press B to enable/disable the rendering of the bounding box.";
            boundingBoxVar.Changed += delegate { Parameter.BoundingBox = boundingBoxVar.Value; };
            boundingBoxVar.Group = generalGroup;


            // Raycasting
            var sampleDistanceVar = new FloatVariable(volumeRenderingBar, Parameter.SampleDistance);
            sampleDistanceVar.Label = "Sample Distance";
            sampleDistanceVar.SetDefinition("min=0.125 max=2.000 step=0.025 precision=3)");
            sampleDistanceVar.Changed += delegate { Parameter.SampleDistance = sampleDistanceVar.Value; };
            sampleDistanceVar.Group = raycastingGroup;

            var techniqueVar = new EnumVariable<Parameter.Technique>(volumeRenderingBar, Parameter.RenderingTechnique);
            techniqueVar.Label = "Technique";
            techniqueVar.Changed += delegate { Parameter.RenderingTechnique = techniqueVar.Value; };
            techniqueVar.Group = raycastingGroup;


            // Shading
            var shadingVar = new BoolVariable(volumeRenderingBar, Parameter.Shading);
            shadingVar.Label = "Shading";
            shadingVar.Changed += delegate { Parameter.Shading = shadingVar.Value; };
            shadingVar.Group = shadingGroup;

            var shadingIntensityVar = new FloatVariable(volumeRenderingBar, Parameter.ShadingIntensity);
            shadingIntensityVar.Label = "Shading Intensity";
            shadingIntensityVar.SetDefinition("min=0.000 max=1.000 step=0.010 precision=3)");
            shadingIntensityVar.Changed += delegate { Parameter.ShadingIntensity = shadingIntensityVar.Value; };
            shadingIntensityVar.Group = shadingGroup;

            var diffuseShadingVar = new FloatVariable(volumeRenderingBar, Parameter.DiffuseShading);
            diffuseShadingVar.Label = "Diffuse Shading";
            diffuseShadingVar.SetDefinition("min=0.000 max=1.000 step=0.020 precision=3)");
            diffuseShadingVar.Changed += delegate { Parameter.DiffuseShading = diffuseShadingVar.Value; };
            diffuseShadingVar.Group = shadingGroup;

            var specularShadingVar = new FloatVariable(volumeRenderingBar, Parameter.SpecularShading);
            specularShadingVar.Label = "Specular Shading";
            specularShadingVar.SetDefinition("min=0.000 max=1.000 step=0.020 precision=3)");
            specularShadingVar.Changed += delegate { Parameter.SpecularShading = specularShadingVar.Value; };
            specularShadingVar.Group = shadingGroup;

            var specularExponentVar = new FloatVariable(volumeRenderingBar, Parameter.SpecularExponent);
            specularExponentVar.Label = "Specular Exponent";
            specularExponentVar.SetDefinition("min=1.000 max=128.000 step=1.000 precision=3)");
            specularExponentVar.Changed += delegate { Parameter.SpecularExponent = specularExponentVar.Value; };
            specularExponentVar.Group = shadingGroup;

            var gradientShadingVar = new FloatVariable(volumeRenderingBar, Parameter.GradientShading);
            gradientShadingVar.Label = "Gradient Shading";
            gradientShadingVar.SetDefinition("min=0.000 max=1.000 step=0.020 precision=3)");
            gradientShadingVar.Changed += delegate { Parameter.GradientShading = gradientShadingVar.Value; };
            gradientShadingVar.Group = shadingGroup;

            var gradientOpacityVar = new FloatVariable(volumeRenderingBar, Parameter.GradientOpacity);
            gradientOpacityVar.Label = "Gradient Opacity";
            gradientOpacityVar.SetDefinition("min=0.000 max=1.000 step=0.020 precision=3)");
            gradientOpacityVar.Changed += delegate { Parameter.GradientOpacity = gradientOpacityVar.Value; };
            gradientOpacityVar.Group = shadingGroup;


            // Transfer Function
            foreach (var transferFunctionNode in Parameter.TransferFunctionNodes)
            {
                var color = transferFunctionNode.Color;
                var controlPoint = transferFunctionNode.ControlPoint;
                var id = transferFunctionNode.Id;

                var tfColorVar = new Color4Variable(volumeRenderingBar, color.R, color.G, color.B, color.A);
                tfColorVar.Label = "TF Color " + id;
                tfColorVar.Changed += delegate
                {
                    int index = Convert.ToInt32(tfColorVar.Label.Split(' ')[2]);
                    TransferFunctionNode tfn = Parameter.TransferFunctionNodes.Find(node => node.Id.Equals(index));
                    tfn.Color = new Color4(tfColorVar.R, tfColorVar.G, tfColorVar.B, tfColorVar.A);
                };
                tfColorVar.Group = transferfunctionGroup;

                var tfColorPointVar = new FloatVariable(volumeRenderingBar, controlPoint);
                tfColorPointVar.Label = "TF Control Point " + id;
                tfColorPointVar.SetDefinition("min=0.000 max=1.000 step=0.001 precision=3)");
                tfColorPointVar.Changed += delegate
                {
                    int index = Convert.ToInt32(tfColorPointVar.Label.Split(' ')[3]);
                    TransferFunctionNode tfn = Parameter.TransferFunctionNodes.Find(node => node.Id.Equals(index));
                    tfn.ControlPoint = tfColorPointVar.Value;
                };
                tfColorPointVar.Group = transferfunctionGroup;
            }


            // Visual Mapping
            var glyphDensityVar = new FloatVariable(volumeRenderingBar, Parameter.GlyphDensity);
            glyphDensityVar.Label = "Glyph Density";
            glyphDensityVar.SetDefinition("min=0.010 max=1.000 step=0.020 precision=3)");
            glyphDensityVar.Changed += delegate { Parameter.GlyphDensity = glyphDensityVar.Value; };
            glyphDensityVar.Group = visualMappingGroup;

            var glyphThresholdVar = new FloatVariable(volumeRenderingBar, Parameter.GlyphThreshold);
            glyphThresholdVar.Label = "Glyph Threshold";
            glyphThresholdVar.SetDefinition("min=0.000 max=1.000 step=0.020 precision=3)");
            glyphThresholdVar.Changed += delegate { Parameter.GlyphThreshold = glyphThresholdVar.Value; };
            glyphThresholdVar.Group = visualMappingGroup;


            generalGroup.Label = "General";
            raycastingGroup.Label = "Raycasting";
            shadingGroup.Label = "Shading";
            transferfunctionGroup.Label = "Transfer Function (TF)";
            visualMappingGroup.Label = "Visual Mapping";
        }

        private void InitializeTransformationBar()
        {
            transformationBar = new Bar(context)
            {
                Label = "Transformation",
                Contained = true,
                ValueColumnWidth = 0,
                Iconified = false,
                Help = "Press SPACE to hide/unhide the 'Transformation'-Bar.",
                Size = new Size(250, 550), // TODO: size and columnwidth
                Position = new Point(windowSize.Width - 250 - (int)(MarginScale * windowSize.Width), (int)(MarginScale * windowSize.Height)) // TODO: position
            };
            transformationBar.SetDefinition("refresh=" + renderPeriod + " color='17 143 129' alpha=64");


            var cameraGroup = new Group(transformationBar);
            var lightGroup = new Group(transformationBar);
            var transformationGroup = new Group(transformationBar);


            // Camera
            fovVar = new FloatVariable(transformationBar, Parameter.FieldOfView);
            fovVar.Label = "Field of View";
            fovVar.SetDefinition("min=30.0 max=90.0 step=0.5 precision=1)");
            fovVar.Help = "Hold Z to zoom with the mouse wheel.";
            fovVar.Changed += delegate { Parameter.FieldOfView = fovVar.Value; };
            fovVar.Group = cameraGroup;

            var cameraPosition = new PointVariable(transformationBar, Parameter.CameraPosition);
            cameraPosition.Group.Label = "Position";
            cameraPosition.Precision = 2;
            cameraPosition.Step = 0.01f;
            cameraPosition.Changed += delegate { Parameter.CameraPosition = new Vector3(cameraPosition.Value.X, cameraPosition.Value.Y, cameraPosition.Value.Z); };
            cameraPosition.Group.Parent = cameraGroup;

            var cameraDirection = new VectorVariable(transformationBar, Parameter.CameraDirection.X, Parameter.CameraDirection.Y, Parameter.CameraDirection.Z);
            cameraDirection.Label = "Direction";
            cameraDirection.SetDefinition("opened=true");
            cameraDirection.ShowValue = true;
            cameraDirection.Changed += delegate { Parameter.CameraDirection = new Vector3(cameraDirection.X, cameraDirection.Y, cameraDirection.Z); };
            cameraDirection.Group = cameraGroup;


            // Light
            var lightPosition = new VectorVariable(transformationBar, Parameter.LightPosition.X, Parameter.LightPosition.Y, Parameter.LightPosition.Z);
            lightPosition.Label = "Position";
            lightPosition.SetDefinition("opened=true");
            lightPosition.ShowValue = true;
            lightPosition.Changed += delegate { Parameter.LightPosition = new Vector3(lightPosition.X, lightPosition.Y, lightPosition.Z); };
            lightPosition.Group = lightGroup;


            // Object
            scaleVar = new FloatVariable(transformationBar, Parameter.Scale);
            scaleVar.Label = "Scale";
            scaleVar.SetDefinition("min=0.10 max=2.00 step=0.01 precision=2)");
            scaleVar.Help = "Hold S to scale the object with the mouse wheel.";
            scaleVar.Changed += delegate { Parameter.Scale = scaleVar.Value; };
            scaleVar.Group = transformationGroup;

            autoRotation = new BoolVariable(transformationBar, Parameter.AutoRotation);
            autoRotation.Label = "Auto Rotation";
            autoRotation.Help = "Press A to enable/disable the auto rotation of the object.";
            autoRotation.Changed += delegate { Parameter.AutoRotation = autoRotation.Value; };
            autoRotation.Group = transformationGroup;

            rotationSpeed = new FloatVariable(transformationBar, Parameter.RotationSpeed);
            rotationSpeed.Label = "Rotation Speed";
            rotationSpeed.SetDefinition("min=-2.00 max=2.00 step=0.05 precision=2)");
            autoRotation.Help = "Press UP/DOWN to increase/decrease the rotation speed.";
            rotationSpeed.Changed += delegate { Parameter.RotationSpeed = rotationSpeed.Value; };
            rotationSpeed.Group = transformationGroup;


            // rotation axis
            //var rotationVar = new QuaternionVariable(volumeRenderingBar);
            rotationVar = new QuaternionVariable(transformationBar, Parameter.Rotation.X, Parameter.Rotation.Y, Parameter.Rotation.Z, Parameter.Rotation.W);
            rotationVar.Label = "Rotation";
            rotationVar.SetDefinition("opened=true");
            rotationVar.ShowValue = true;
            rotationVar.Help = "Hold R to rotate the object with the mouse.";
            rotationVar.Changed += delegate
            {
                Parameter.Rotation = new Quaternion(rotationVar.X, rotationVar.Y, rotationVar.Z, MathHelper.Pi * rotationVar.W);
                Parameter.RotationMatrix = Matrix4.CreateFromQuaternion(Parameter.Rotation);
            };
            rotationVar.Group = transformationGroup;

            var translationVar = new VectorVariable(transformationBar);
            translationVar.Label = "Translation";
            translationVar.SetDefinition("opened=true");
            translationVar.ShowValue = true;
            translationVar.Changed += delegate { Parameter.Translation = new Vector3(translationVar.X, translationVar.Y, translationVar.Z); };
            translationVar.Group = transformationGroup;


            cameraGroup.Label = "Camera";
            lightGroup.Label = "Light";
            transformationGroup.Label = "Object";
        }


        #region Minor event handling

        public void OnRenderFrame()
        {
            context.Draw();
        }

        public void OnResize(Size clientSize)
        {
            context.HandleResize(clientSize);
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
            }
        }

        public void OnMouseDown(MouseButtonEventArgs e)
        {
            // Mouse Down position
            orgX = e.X;
            orgY = e.Y;

            if (HandleMouseClick(context, e))
            {
                return;
            }
        }

        public void OnMouseUp(MouseButtonEventArgs e)
        {
            // Save accumulated rotation
            if (rotationEnabled)
            {
                preRotX += rotX;
                preRotY -= rotY;
            }

            if (HandleMouseClick(context, e))
            {
                return;
            }
        }

        public void OnMouseMove(MouseMoveEventArgs e)
        {
            if (rotationEnabled && e.Mouse.IsButtonDown(MouseButton.Left))
            {
                rotX = e.X - orgX;
                rotY = e.Y - orgY;

                Matrix4 x = Matrix4.CreateRotationX((preRotY + rotY) / 10.0f);
                Matrix4 y = Matrix4.CreateRotationY((preRotX + rotX) / 10.0f);

                Parameter.RotationMatrix = x * y;

                Matrix3 rotation = new Matrix3(Parameter.RotationMatrix);
                Parameter.Rotation = Quaternion.FromMatrix(rotation);
                rotationVar.X = Parameter.Rotation.X;
                rotationVar.Y = Parameter.Rotation.Y;
                rotationVar.Z = Parameter.Rotation.Z;
                rotationVar.W = Parameter.Rotation.W;
            }

            if (context.HandleMouseMove(e.Position))
            {
                return;
            }
        }

        public void OnMouseWheel(MouseWheelEventArgs e)
        {
            var scale = Parameter.Scale + e.Delta * ScaleStep;
            if (scaleEnabled && scale >= MinScale && scale <= MaxScale)
            {
                scaleVar.Value = scale;
                Parameter.Scale = scale;
            }

            var fov = Parameter.FieldOfView - e.Delta * FovStep;
            if (zoomEnabled && fov >= MinFov && fov <= MaxFov)
            {
                fovVar.Value = fov;
                Parameter.FieldOfView = fov;
            }

            if (context.HandleMouseWheel(e.Value))
            {
                return;
            }
        }


        public void OnKeyDown(KeyboardKeyEventArgs e)
        {
            // Shortcut to iconify the volumeRenderingBar and the transformationBar
            if (e.Key.Equals(OpenTK.Input.Key.Space))
            {
                volumeRenderingBar.Iconified = !volumeRenderingBar.Iconified;
                transformationBar.Iconified = !transformationBar.Iconified;
            }
            // Shortcut to enable/disable the bounding box
            if (e.Key.Equals(OpenTK.Input.Key.B))
            {
                boundingBoxVar.Value = !Parameter.BoundingBox;
                Parameter.BoundingBox = !Parameter.BoundingBox;
            }
            // Bool to check if auto rotation is enabled
            if (e.Key.Equals(OpenTK.Input.Key.A))
            {
                Parameter.AutoRotation = !Parameter.AutoRotation;
                autoRotation.Value = Parameter.AutoRotation;
            }
            // Bool to check if scale is allowed
            if (e.Key.Equals(OpenTK.Input.Key.S))
            {
                scaleEnabled = true;
            }
            // Bool to check if rotation is allowed
            if (e.Key.Equals(OpenTK.Input.Key.R))
            {
                rotationEnabled = true;
            }
            // Bool to check if zoom is allowed
            if (e.Key.Equals(OpenTK.Input.Key.Y)) // Mind: OpenTK uses QWERTY layout so Y is Z on a QWERTZ keyboard
            {
                zoomEnabled = true;
            }
            // Increase auto rotation speed
            if (e.Key.Equals(OpenTK.Input.Key.Up))
            {
                if (rotationSpeed.Value + rotationSpeed.Step <= rotationSpeed.Max)
                {
                    rotationSpeed.Value += rotationSpeed.Step;
                    Parameter.RotationSpeed = rotationSpeed.Value;
                }
            }
            // Decrease auto rotation speed
            if (e.Key.Equals(OpenTK.Input.Key.Down))
            {
                if (rotationSpeed.Value - rotationSpeed.Step >= rotationSpeed.Min)
                {
                    rotationSpeed.Value -= rotationSpeed.Step;
                    Parameter.RotationSpeed = rotationSpeed.Value;
                }
            }


            if (HandleKeyInput(context, e, true))
            {
                return;
            }
        }

        public void OnKeyUp(KeyboardKeyEventArgs e)
        {
            // Bool to check if scale is allowed
            if (e.Key.Equals(OpenTK.Input.Key.S))
            {
                scaleEnabled = false;
            }
            // Bool to check if rotation is allowed
            if (e.Key.Equals(OpenTK.Input.Key.R))
            {
                rotationEnabled = false;
            }
            // Bool to check if zoom is allowed
            if (e.Key.Equals(OpenTK.Input.Key.Y)) // Mind: OpenTK uses QWERTY layout so Y is Z on a QWERTZ keyboard
            {
                zoomEnabled = false;
            }

            if (HandleKeyInput(context, e, false))
            {
                return;
            }
        }

        public void OnKeyPress(KeyPressEventArgs e)
        {
            if (context.HandleKeyPress(e.KeyChar))
            {
                return;
            }
        }

        #endregion

        // ATTENTION: COPIED FROM https://github.com/TomCrypto/AntTweakBar.NET/blob/master/Samples/OpenTK-OpenGL/Program.cs
        #region Event Conversion

        // Because OpenTK does not use an event loop, the native AntTweakBar library
        // has no provisions for directly handling user events. Therefore we need to
        // convert any OpenTK events to AntTweakBar events before handling them.

        private static bool HandleMouseClick(Context context, MouseButtonEventArgs e)
        {
            IDictionary<OpenTK.Input.MouseButton, Tw.MouseButton> mapping = new Dictionary<OpenTK.Input.MouseButton, Tw.MouseButton>()
            {
                { OpenTK.Input.MouseButton.Left, Tw.MouseButton.Left },
                { OpenTK.Input.MouseButton.Middle, Tw.MouseButton.Middle },
                { OpenTK.Input.MouseButton.Right, Tw.MouseButton.Right },
            };

            var action = e.IsPressed ? Tw.MouseAction.Pressed : Tw.MouseAction.Released;

            if (mapping.ContainsKey(e.Button))
            {
                return context.HandleMouseClick(action, mapping[e.Button]);
            }
            else
            {
                return false;
            }
        }

        private static bool HandleKeyInput(Context context, KeyboardKeyEventArgs e, bool down)
        {
            IDictionary<OpenTK.Input.Key, Tw.Key> mapping = new Dictionary<OpenTK.Input.Key, Tw.Key>()
            {
                { OpenTK.Input.Key.BackSpace, Tw.Key.Backspace },
                { OpenTK.Input.Key.Tab, Tw.Key.Tab },
                { OpenTK.Input.Key.Clear, Tw.Key.Clear },
                { OpenTK.Input.Key.Enter, Tw.Key.Return },
                { OpenTK.Input.Key.Pause, Tw.Key.Pause },
                { OpenTK.Input.Key.Escape, Tw.Key.Escape },
                { OpenTK.Input.Key.Space, Tw.Key.Space },
                { OpenTK.Input.Key.Delete, Tw.Key.Delete },
                { OpenTK.Input.Key.Up, Tw.Key.Up },
                { OpenTK.Input.Key.Down, Tw.Key.Down },
                { OpenTK.Input.Key.Right, Tw.Key.Right },
                { OpenTK.Input.Key.Left, Tw.Key.Left },
                { OpenTK.Input.Key.Insert, Tw.Key.Insert },
                { OpenTK.Input.Key.Home, Tw.Key.Home },
                { OpenTK.Input.Key.End, Tw.Key.End },
                { OpenTK.Input.Key.PageUp, Tw.Key.PageUp },
                { OpenTK.Input.Key.PageDown, Tw.Key.PageDown },
                { OpenTK.Input.Key.F1, Tw.Key.F1 },
                { OpenTK.Input.Key.F2, Tw.Key.F2 },
                { OpenTK.Input.Key.F3, Tw.Key.F3 },
                { OpenTK.Input.Key.F4, Tw.Key.F4 },
                { OpenTK.Input.Key.F5, Tw.Key.F5 },
                { OpenTK.Input.Key.F6, Tw.Key.F6 },
                { OpenTK.Input.Key.F7, Tw.Key.F7 },
                { OpenTK.Input.Key.F8, Tw.Key.F8 },
                { OpenTK.Input.Key.F9, Tw.Key.F9 },
                { OpenTK.Input.Key.F10, Tw.Key.F10 },
                { OpenTK.Input.Key.F11, Tw.Key.F11 },
                { OpenTK.Input.Key.F12, Tw.Key.F12 },
                { OpenTK.Input.Key.F13, Tw.Key.F13 },
                { OpenTK.Input.Key.F14, Tw.Key.F14 },
                { OpenTK.Input.Key.F15, Tw.Key.F15 },
                { OpenTK.Input.Key.A, Tw.Key.A },
                { OpenTK.Input.Key.B, Tw.Key.B },
                { OpenTK.Input.Key.C, Tw.Key.C },
                { OpenTK.Input.Key.D, Tw.Key.D },
                { OpenTK.Input.Key.E, Tw.Key.E },
                { OpenTK.Input.Key.F, Tw.Key.F },
                { OpenTK.Input.Key.G, Tw.Key.G },
                { OpenTK.Input.Key.H, Tw.Key.H },
                { OpenTK.Input.Key.I, Tw.Key.I },
                { OpenTK.Input.Key.J, Tw.Key.J },
                { OpenTK.Input.Key.K, Tw.Key.K },
                { OpenTK.Input.Key.L, Tw.Key.L },
                { OpenTK.Input.Key.M, Tw.Key.M },
                { OpenTK.Input.Key.N, Tw.Key.N },
                { OpenTK.Input.Key.O, Tw.Key.O },
                { OpenTK.Input.Key.P, Tw.Key.P },
                { OpenTK.Input.Key.Q, Tw.Key.Q },
                { OpenTK.Input.Key.R, Tw.Key.R },
                { OpenTK.Input.Key.S, Tw.Key.S },
                { OpenTK.Input.Key.T, Tw.Key.T },
                { OpenTK.Input.Key.U, Tw.Key.U },
                { OpenTK.Input.Key.V, Tw.Key.V },
                { OpenTK.Input.Key.W, Tw.Key.W },
                { OpenTK.Input.Key.X, Tw.Key.X },
                { OpenTK.Input.Key.Y, Tw.Key.Y },
                { OpenTK.Input.Key.Z, Tw.Key.Z },
            };

            var modifiers = Tw.KeyModifiers.None;
            if (e.Modifiers.HasFlag(KeyModifiers.Alt))
                modifiers |= Tw.KeyModifiers.Alt;
            if (e.Modifiers.HasFlag(KeyModifiers.Shift))
                modifiers |= Tw.KeyModifiers.Shift;
            if (e.Modifiers.HasFlag(KeyModifiers.Control))
                modifiers |= Tw.KeyModifiers.Ctrl;

            if (mapping.ContainsKey(e.Key))
            {
                if (down)
                {
                    return context.HandleKeyDown(mapping[e.Key], modifiers);
                }
                else
                {
                    return context.HandleKeyUp(mapping[e.Key], modifiers);
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }

    sealed class PointVariable : StructVariable<Vector3>
    {
        private FloatVariable x { get { return (variables[0] as FloatVariable); } }
        private FloatVariable y { get { return (variables[1] as FloatVariable); } }
        private FloatVariable z { get { return (variables[2] as FloatVariable); } }


        public PointVariable(Bar bar, Vector3 point)
            : base(bar, new FloatVariable(bar, point.X), new FloatVariable(bar, point.Y), new FloatVariable(bar, point.Z))
        {
            x.Label = "X";
            y.Label = "Y";
            z.Label = "Z";
        }

        public override Vector3 Value
        {
            get { return new Vector3(x.Value, y.Value, z.Value); }
            set
            {
                x.Value = value.X;
                y.Value = value.Y;
                z.Value = value.Z;
            }
        }

        public float Step
        {
            get { return x.Step; }
            set
            {
                x.Step = value;
                y.Step = value;
                z.Step = value;
            }
        }

        public float Precision
        {
            get { return x.Precision; }
            set
            {
                x.Precision = value;
                y.Precision = value;
                z.Precision = value;
            }
        }
    }
}