﻿using OpenTK.Graphics;

namespace VolumeChop.Renderer
{
    class TransferFunctionNode
    {
        public Color4 Color { get; set; }
        public float ControlPoint { get; set; }

        public int Id { get; private set; }

        public TransferFunctionNode(Color4 color, float controlPoint, int id)
        {
            Color = color;
            ControlPoint = controlPoint;
            Id = id;
        }
    }
}