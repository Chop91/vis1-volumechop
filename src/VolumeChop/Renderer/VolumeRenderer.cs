﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using EnableCap = OpenTK.Graphics.OpenGL.EnableCap;
using GL = OpenTK.Graphics.OpenGL.GL;

namespace VolumeChop.Renderer
{
    class VolumeRenderer : GameWindow
    {
        private float accumulatedDeltaTime;
        private Volume volume;
        private GUI gui;

        private Parameter.DataSet dataSet;
        private Parameter.Technique technique;

        public VolumeRenderer()
            : base(400, 400, new GraphicsMode(32, 24, 0, 4))
        {
            int windowWidth = Convert.ToInt32(ConfigurationManager.AppSettings["windowWidth"]);
            int windowHeight = Convert.ToInt32(ConfigurationManager.AppSettings["windowHeight"]);
            bool fullscreen = Convert.ToBoolean(ConfigurationManager.AppSettings["fullscreen"]);
            int renderFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["renderFrequency"]);
            int updateFrequency = Convert.ToInt32(ConfigurationManager.AppSettings["updateFrequency"]);

            ClientSize = new Size(windowWidth, windowHeight);

            TargetRenderFrequency = renderFrequency;
            TargetUpdateFrequency = updateFrequency;

            if (fullscreen)
            {
                WindowState = WindowState.Fullscreen;
            }
        }


        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            Title = "VolumeChop";
            GL.ClearColor(0, 0, 0, 0);
            GL.Enable(EnableCap.DepthTest);

            WindowBorder = WindowBorder.Fixed;

            gui = new GUI(ClientSize, TargetRenderPeriod);

            volume = new Volume(ClientSize);
            volume.Parameter = gui.Parameter;

            dataSet = gui.Parameter.Data;
            technique = gui.Parameter.RenderingTechnique;
            CreateVolume();
        }

        public void CreateVolume()
        {
            // Always store your volume data there
            string path = @"./Data/VolumeData/";
            string filename = "";

            if (dataSet.Equals(Parameter.DataSet.Lobster))
            {
                filename = @"lobster_120x120x34.dat";
            }
            else if (dataSet.Equals(Parameter.DataSet.BeetleSmall))
            {
                filename = @"beetle_138x138x82.dat";
            }
            else if (dataSet.Equals(Parameter.DataSet.Beetle))
            {
                filename = @"beetle_277x277x164.dat";
            }
            else if (dataSet.Equals(Parameter.DataSet.HeadSmall))
            {
                filename = @"head_128x128x112.dat";
            }
            else if (dataSet.Equals(Parameter.DataSet.Head))
            {
                filename = @"head_256x256x225.dat";
            }

            volume.Create(Path.Combine(path, filename));
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            float deltaTime = Convert.ToSingle(e.Time);
            accumulatedDeltaTime += deltaTime;

            // Update title every 10 updates
            if (accumulatedDeltaTime > 10 * TargetUpdatePeriod)
            {
                Title = String.Format("{0} @ " + Math.Round(RenderFrequency, 1) + "fps", ConfigurationManager.AppSettings["windowName"]);
                accumulatedDeltaTime = 0.0f;
            }

            volume.OnUpdateFrame(deltaTime);

            if (!dataSet.Equals(gui.Parameter.Data))
            {
                dataSet = gui.Parameter.Data;
                gui.InitializeParameters(dataSet);
                CreateVolume();
            }

            if (!technique.Equals(gui.Parameter.RenderingTechnique))
            {
                technique = gui.Parameter.RenderingTechnique;
                volume.ChooseShader();
            }
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            volume.OnRenderFrame();

            gui.OnRenderFrame();

            SwapBuffers();
        }












        #region Minor event handling

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            GL.Viewport(this.ClientRectangle);
            gui.OnResize(ClientSize);
        }

        protected override void Dispose(bool manual)
        {
            gui.Dispose();
            base.Dispose(manual);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            gui.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            gui.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);
            gui.OnMouseMove(e);
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            gui.OnMouseWheel(e);
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);

            gui.OnKeyDown(e);

            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            base.OnKeyUp(e);
            gui.OnKeyUp(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            gui.OnKeyPress(e);
        }

        #endregion
    }
}