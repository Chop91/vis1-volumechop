﻿using OpenTK.Graphics.OpenGL4;
using System.Diagnostics;
using System.IO;

namespace VolumeChop.Renderer
{
    class Shader
    {
        private int programHandle;
        private int vertexHandle;
        private int geometryHandle;
        private int fragmentHandle;

        public Shader(string vertexShader, string fragmentShader)
            : this(vertexShader, null, fragmentShader)
        { }

        public Shader(string vertexShader, string geometryShader, string fragmentShader)
        {
            programHandle = 0;
            vertexHandle = 0;
            geometryHandle = 0;
            fragmentHandle = 0;
            Trace.TraceInformation("Creating Shaders: {0} | {1} | {2}", vertexShader, geometryShader, fragmentShader);

            programHandle = GL.CreateProgram();

            if (ProgramHandle == 0)
            {
                Trace.TraceError(GL.GetProgramInfoLog(ProgramHandle));
                System.Environment.Exit(VolumeChop.ExitFailure);
            }

            LoadShader(vertexShader, ShaderType.VertexShader, ProgramHandle, out vertexHandle);

            if (geometryShader != null)
            {
                LoadShader(geometryShader, ShaderType.GeometryShader, ProgramHandle, out geometryHandle);
            }

            LoadShader(fragmentShader, ShaderType.FragmentShader, ProgramHandle, out fragmentHandle);

            Link();
        }

        public void UseShader()
        {
            GL.UseProgram(ProgramHandle);
        }

        private void LoadShader(string shader, ShaderType shaderType, int handle, out int adress)
        {
            adress = GL.CreateShader(shaderType);

            using (StreamReader reader = new StreamReader(shader))
            {
                GL.ShaderSource(adress, reader.ReadToEnd());
            }

            GL.CompileShader(adress);
            GL.AttachShader(handle, adress);
            Trace.TraceInformation("Load {0}: {1}", shaderType.ToString(), GL.GetShaderInfoLog(adress));
        }

        private void Link()
        {
            GL.LinkProgram(ProgramHandle);
            Trace.TraceInformation("Link: {0}", GL.GetProgramInfoLog(ProgramHandle));
        }

        public int ProgramHandle
        {
            get { return programHandle; }
        }
    }
}