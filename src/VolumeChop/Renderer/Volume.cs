﻿
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VolumeChop.Renderer.Geometry;
using VolumeChop.Util;
using BlendingFactorDest = OpenTK.Graphics.OpenGL4.BlendingFactorDest;
using BlendingFactorSrc = OpenTK.Graphics.OpenGL4.BlendingFactorSrc;
using ClearBufferMask = OpenTK.Graphics.OpenGL4.ClearBufferMask;
using CullFaceMode = OpenTK.Graphics.OpenGL4.CullFaceMode;
using DrawElementsType = OpenTK.Graphics.OpenGL4.DrawElementsType;
using EnableCap = OpenTK.Graphics.OpenGL4.EnableCap;
using GL = OpenTK.Graphics.OpenGL4.GL;
using MaterialFace = OpenTK.Graphics.OpenGL4.MaterialFace;
using PixelFormat = OpenTK.Graphics.OpenGL4.PixelFormat;
using PixelInternalFormat = OpenTK.Graphics.OpenGL4.PixelInternalFormat;
using PixelType = OpenTK.Graphics.OpenGL4.PixelType;
using PolygonMode = OpenTK.Graphics.OpenGL4.PolygonMode;
using TextureMagFilter = OpenTK.Graphics.OpenGL4.TextureMagFilter;
using TextureMinFilter = OpenTK.Graphics.OpenGL4.TextureMinFilter;
using TextureParameterName = OpenTK.Graphics.OpenGL4.TextureParameterName;
using TextureTarget = OpenTK.Graphics.OpenGL4.TextureTarget;
using TextureUnit = OpenTK.Graphics.OpenGL4.TextureUnit;
using TextureWrapMode = OpenTK.Graphics.OpenGL4.TextureWrapMode;

namespace VolumeChop.Renderer
{
    class Volume
    {
        // ASSUMPTION: We are using just 12 Bit so 4095 is the max value
        private const ushort MaxValue = 4095;

        private Matrix4 projection;
        private Matrix4 view;
        private Matrix4 viewProjection;
        private Matrix4 mvp;
        private Matrix4 autoRotation;

        private Shader rayCasting;
        private Shader mip;
        private Shader dvr;
        private Shader average;
        private Shader colorCube;
        private Shader boundingBoxShader;
        private Shader glypShader;

        private FBO frontFaceFbo;
        private FBO backFaceFbo;

        public Parameter Parameter { get; set; }

        private uint volumeTexture;
        private uint transferFunctionTexture;
        private uint gradientTexture;

        // Header
        private ushort sizeX;
        private ushort sizeY;
        private ushort sizeZ;
        private ushort maxSize;

        // Body
        private List<float> voxels;

        private List<float> gradients;
        private List<Vector3> gradients2;
        private float maxMagn;

        private Sphere glyph;
        private Cube boundingBox;
        private Quad quad;


        private Size windowSize;

        private Task loadDataTask;

        public Volume(Size windowSize)
        {
            colorCube = new Shader("./Shader/colorcube.vert", "./Shader/colorcube.frag");
            boundingBoxShader = new Shader("./Shader/boundingbox.vert", "./Shader/boundingbox.frag");
            glypShader = new Shader("./Shader/glyph.vert", "./Shader/glyph.frag");

            mip = new Shader("./Shader/mip.vert", "./Shader/mip.frag");
            dvr = new Shader("./Shader/dvr.vert", "./Shader/dvr.frag");
            average = new Shader("./Shader/average.vert", "./Shader/average.frag");

            glyph = new Sphere();
            boundingBox = new Cube();
            quad = new Quad();

            this.windowSize = windowSize;

            frontFaceFbo = new FBO(windowSize.Width, windowSize.Height);
            backFaceFbo = new FBO(windowSize.Width, windowSize.Height);
        }

        public void ChooseShader()
        {
            Console.WriteLine("Loading Shader...");

            if (Parameter.RenderingTechnique.Equals(Parameter.Technique.MIP))
            {
                rayCasting = mip;
            }
            else if (Parameter.RenderingTechnique.Equals(Parameter.Technique.DVR))
            {
                rayCasting = dvr;
            }
            else if (Parameter.RenderingTechnique.Equals(Parameter.Technique.Average))
            {
                rayCasting = average;
            }

            Console.WriteLine("Finished loading: " + Parameter.RenderingTechnique.ToString());
            Console.WriteLine();
        }

        public void Create(string path)
        {
            loadDataTask = Task.Factory.StartNew(() => LoadData(path));
            loadDataTask.Wait();

            GenerateTextures();
            ChooseShader();

            frontFaceFbo.CreateFbo();
            backFaceFbo.CreateFbo();

            Matrix4.CreatePerspectiveFieldOfView(Parameter.FieldOfView * ((float)Math.PI / 180.0f), (float)windowSize.Width / (float)windowSize.Height, 0.5f, 20.0f, out projection);
            view = Matrix4.LookAt(Parameter.CameraPosition, Parameter.CameraPosition + Parameter.CameraDirection, Vector3.UnitY);
            viewProjection = view * projection;

            autoRotation = Matrix4.Identity;
        }

        public void LoadData(string path)
        {
            Console.WriteLine("Loading DataSet...");
            Trace.TraceInformation("Loading DataSet...");

            try
            {
                var fileStream = new FileStream(path, FileMode.Open);
                var reader = new BinaryReader(fileStream);

                // Get header data
                // ASSUMPTION: the first three numbers are the size in X, Y and Z direction
                sizeX = reader.ReadUInt16();
                sizeY = reader.ReadUInt16();
                sizeZ = reader.ReadUInt16();

                // Get maxSize
                maxSize = Math.Max(sizeX, Math.Max(sizeY, sizeZ));

                // #voxels
                int voxelCount = sizeX * sizeY * sizeZ;
                int i = 0;

                // Get the volume data
                voxels = new List<float>(voxelCount);
                using (var progressBar = new ProgressBar())
                {
                    // Read 2 bytes and interpret them as ushorts
                    while (reader.BaseStream.Position < reader.BaseStream.Length)
                    {
                        var value = reader.ReadUInt16();

                        // Normalize the value and clamp it to [0.0, 1.0]
                        voxels.Add(Math.Min(1.0f, Math.Max(value / (float)MaxValue, 0.0f)));

                        // Update progress bar
                        progressBar.Report((double)i / voxelCount);
                        i++;
                    }
                }

                fileStream.Close();
            }
            catch (Exception e)
            {
                Trace.TraceError("An error occurred: " + e.Message
                    + "\n======= STACK TRACE =======\n"
                    + e.StackTrace);
            }

            Trace.TraceInformation("Finished loading: {0}", Path.GetFileName(path));
            Trace.TraceInformation("size: {0}x{1}x{2} | voxels: {3}", sizeX, sizeY, sizeZ, voxels.Count);
            Console.WriteLine("Finished loading: {0}", Path.GetFileName(path));
            Console.WriteLine("size: {0}x{1}x{2} | voxels: {3}", sizeX, sizeY, sizeZ, voxels.Count);
            Console.WriteLine();
        }

        public void GenerateTextures()
        {
            // Load data into a 3D texture
            GL.GenTextures(1, out volumeTexture);
            GL.BindTexture(TextureTarget.Texture3D, volumeTexture);

            // Set the texture parameters
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            GL.TexImage3D(TextureTarget.Texture3D, 0, PixelInternalFormat.Luminance, sizeX, sizeY, sizeZ, 0, PixelFormat.Luminance, PixelType.Float, voxels.ToArray());


            GL.BindTexture(TextureTarget.Texture3D, 0);

            // transfer function
            GL.GenTextures(1, out transferFunctionTexture);
            GenerateTransferfunction();

            // central difference
            gradients = new List<float>(sizeX * sizeY * sizeZ * 3);
            gradients2 = new List<Vector3>(sizeX * sizeY * sizeZ);
            for (int z = 0; z < sizeZ; z++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    for (int x = 0; x < sizeX; x++)
                    {
                        float current = voxels.ElementAt(x + y * sizeX + z * sizeX * sizeY);

                        // Edge treatment - ElementAtOrDefault sets values where the index is out of range to 0.0f
                        float plusX = voxels.ElementAtOrDefault((x + 1) + y * sizeX + z * sizeX * sizeY);
                        float minusX = voxels.ElementAtOrDefault((x - 1) + y * sizeX + z * sizeX * sizeY);

                        float plusY = voxels.ElementAtOrDefault(x + (y + 1) * sizeX + z * sizeX * sizeY);
                        float minusY = voxels.ElementAtOrDefault(x + (y - 1) * sizeX + z * sizeX * sizeY);

                        float plusZ = voxels.ElementAtOrDefault(x + y * sizeX + (z + 1) * sizeX * sizeY);
                        float minusZ = voxels.ElementAtOrDefault(x + y * sizeX + (z - 1) * sizeX * sizeY);

                        // Calculate the gradients
                        float gradientX = (plusX - minusX) / 2.0f;
                        float gradientY = (plusY - minusY) / 2.0f;
                        float gradientZ = (plusZ - minusZ) / 2.0f;

                        gradients.Add(gradientX);
                        gradients.Add(gradientY);
                        gradients.Add(gradientZ);

                        Vector3 gradient = new Vector3(gradientX, gradientY, gradientZ);
                        gradients2.Add(gradient);
                        maxMagn = Math.Max(maxMagn, gradient.Length);
                    }
                }
            }



            // Load data into a 3D texture
            GL.GenTextures(1, out gradientTexture);
            GL.BindTexture(TextureTarget.Texture3D, gradientTexture);

            // Set the texture parameters
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture3D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            // MIND: need to do this because ToArrays() causes OutOfMemory Exception,
            // also it is not possible to have gradient and density in one texture
            // because not enougth video memory
            float[] array = new float[gradients.Count];
            for (int i = 0; i < gradients.Count; i++)
            {
                array[i] = gradients[i];
            }

            GL.TexImage3D(TextureTarget.Texture3D, 0, PixelInternalFormat.Rgb16f, sizeX, sizeY, sizeZ, 0, PixelFormat.Rgb, PixelType.Float, array);

            GL.BindTexture(TextureTarget.Texture3D, 0);
        }

        private void GenerateTransferfunction()
        {
            GL.BindTexture(TextureTarget.Texture1D, transferFunctionTexture);

            List<float> color = new List<float>();

            for (int i = 0; i < 4096; i++)
            {
                float density = i / 4096.0f;
                float[] col = new float[4];

                // Assume point0 < point1
                for (int j = 0; j < Parameter.TransferFunctionNodes.Count - 1; j++)
                {
                    TransferFunctionNode tfn0 = Parameter.TransferFunctionNodes[j];
                    TransferFunctionNode tfn1 = Parameter.TransferFunctionNodes[j + 1];

                    if (density >= tfn0.ControlPoint)
                    {
                        if (density < tfn1.ControlPoint && j < Parameter.TransferFunctionNodes.Count - 1)
                        {
                            LinearInterpolation(tfn0.ControlPoint, tfn1.ControlPoint, tfn0.Color, tfn1.Color, density,
                                out col);
                            break;
                        }
                        // Handle last control point
                        else if (density <= tfn1.ControlPoint && j == Parameter.TransferFunctionNodes.Count - 1)
                        {
                            LinearInterpolation(tfn0.ControlPoint, tfn1.ControlPoint, tfn0.Color, tfn1.Color, density,
                                out col);
                            break;
                        }
                    }
                }

                color.AddRange(col);
            }

            GL.TexImage1D(TextureTarget.Texture1D, 0, PixelInternalFormat.Rgba16, 4096, 0, PixelFormat.Rgba, PixelType.Float, color.ToArray());

            GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            GL.BindTexture(TextureTarget.Texture1D, 0);
        }

        // Assume point0 < point1
        private void LinearInterpolation(float point0, float point1, Color4 color0, Color4 color1, float density, out float[] color)
        {
            // Scaled the density for the interval between point0 and point1 to [0, 1]
            float intervalDensity = (density - point0) / (point1 - point0);

            // Linear interpolation: (1-f) * color0 + f * color1
            float r = (1 - intervalDensity) * color0.R + intervalDensity * color1.R;
            float g = (1 - intervalDensity) * color0.G + intervalDensity * color1.G;
            float b = (1 - intervalDensity) * color0.B + intervalDensity * color1.B;
            float a = (1 - intervalDensity) * color0.A + intervalDensity * color1.A;

            color = new float[4];
            color[0] = Math.Min(1.0f, Math.Max(r, 0.0f));
            color[1] = Math.Min(1.0f, Math.Max(g, 0.0f)); ;
            color[2] = Math.Min(1.0f, Math.Max(b, 0.0f)); ;
            color[3] = Math.Min(1.0f, Math.Max(a, 0.0f)); ;
        }

        public void OnUpdateFrame(float deltaTime)
        {
            if (loadDataTask.IsCompleted)
            {
                boundingBox.ResetModelMatrix();

                // Translate
                boundingBox.Translate(Parameter.Translation);

                // Rotate
                boundingBox.Rotate(Parameter.RotationMatrix);

                // Apply auto-rotation with delta time for frame rate independency
                if (Parameter.AutoRotation)
                {
                    autoRotation *= Matrix4.CreateRotationZ((float)(Math.PI / 2.0f) * Parameter.RotationSpeed * deltaTime);
                }
                boundingBox.Rotate(autoRotation);

                // Scale
                boundingBox.Scale(new Vector3((float)sizeX / (float)maxSize, (float)sizeY / (float)maxSize,
                    (float)sizeZ / (float)maxSize));
                boundingBox.Scale(new Vector3(Parameter.Scale));


                Matrix4.CreatePerspectiveFieldOfView(Parameter.FieldOfView * ((float)Math.PI / 180.0f), (float)windowSize.Width / (float)windowSize.Height, 0.5f, 20.0f, out projection);
                view = Matrix4.LookAt(Parameter.CameraPosition, Parameter.CameraPosition + Parameter.CameraDirection, Vector3.UnitY);
                viewProjection = view * projection;

                GenerateTransferfunction();

                // Calculate MVP matrix of bounding box
                Matrix4 model = boundingBox.ModelMatrix;
                mvp = model * viewProjection;
            }
        }

        public void OnRenderFrame()
        {
            if (loadDataTask.IsCompleted)
            {
                if (Parameter.RenderingTechnique.Equals(Parameter.Technique.Glyph))
                {
                    GlyphRendering();
                }
                else
                {
                    VolumeRendering();
                }
            }
        }

        private void VolumeRendering()
        {
            // render front face of bounding box
            #region first pass

            frontFaceFbo.BindFbo();

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            colorCube.UseShader();

            GL.BindVertexArray(boundingBox.Mesh.Vao);

            GL.UniformMatrix4(GL.GetUniformLocation(colorCube.ProgramHandle, "u_MVP"), false, ref mvp);

            GL.DrawElements(PrimitiveType.Quads, boundingBox.Mesh.Indices.Count,
                DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.BindVertexArray(0);

            frontFaceFbo.UnbindFbo();

            #endregion

            // render back face of bounding box
            #region second pass

            backFaceFbo.BindFbo();

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            colorCube.UseShader();

            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);

            GL.BindVertexArray(boundingBox.Mesh.Vao);

            GL.UniformMatrix4(GL.GetUniformLocation(colorCube.ProgramHandle, "u_MVP"), false, ref mvp);

            GL.DrawElements(PrimitiveType.Quads, boundingBox.Mesh.Indices.Count,
                DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.BindVertexArray(0);

            GL.CullFace(CullFaceMode.Back);
            GL.Disable(EnableCap.CullFace);

            backFaceFbo.UnbindFbo();

            #endregion

            // render bounding box
            #region third pass

            if (Parameter.BoundingBox)
            {
                BoundingBoxRendering(ref mvp);
            }

            #endregion

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.DstAlpha);

            // raycasting
            #region fourth pass

            if (!Parameter.BoundingBox)
            {
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            }

            rayCasting.UseShader();

            GL.BindVertexArray(quad.Vao);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, frontFaceFbo.ColorTextures[0]);
            GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_texture_front"), 0);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, backFaceFbo.ColorTextures[0]);
            GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_texture_back"), 1);

            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture3D, volumeTexture);
            GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_texture_volume"), 2);

            GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_sample_distance"), Parameter.SampleDistance);
            GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_unit_step"), 1.0f / maxSize);

            if (Parameter.RenderingTechnique.Equals(Parameter.Technique.DVR))
            {
                GL.ActiveTexture(TextureUnit.Texture3);
                GL.BindTexture(TextureTarget.Texture1D, transferFunctionTexture);
                GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_texture_transfer_function"), 3);

                GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_shading"), Convert.ToInt16(Parameter.Shading));
                GL.Uniform3(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_light_position"), Parameter.LightPosition);
                GL.Uniform3(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_volume_position"), boundingBox.ModelMatrix.ExtractTranslation());


                if (Parameter.Shading)
                {
                    GL.ActiveTexture(TextureUnit.Texture4);
                    GL.BindTexture(TextureTarget.Texture3D, gradientTexture);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_texture_gradient"), 4);

                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_diffuse_shading"), Parameter.DiffuseShading);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_specular_shading"), Parameter.SpecularShading);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_specular_exponent"), Parameter.SpecularExponent);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_shading_intensity"), Parameter.ShadingIntensity);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_gradient_shading"), Parameter.GradientShading);
                    GL.Uniform1(GL.GetUniformLocation(rayCasting.ProgramHandle, "u_gradient_opacity"), Parameter.GradientOpacity);
                }
            }

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.BindVertexArray(0);

            #endregion

            GL.Disable(EnableCap.Blend);
        }

        private void GlyphRendering()
        {
            // render bounding box
            #region first pass

            if (!Parameter.BoundingBox)
            {
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            }
            else
            {
                BoundingBoxRendering(ref mvp);
            }

            #endregion

            // render glyphs
            #region second pass

            float glyphDensity = Parameter.GlyphDensity;
            float glyphThreshold = Parameter.GlyphThreshold;

            // Bounding box from (-1, -1, -1) to (1, 1, 1)
            float minimum = -1.0f;
            float maximum = 1.0f;

            float startX = minimum;
            float startY = minimum;
            float startZ = minimum;

            float rangeX = maximum - startX;
            float rangeY = maximum - startY;
            float rangeZ = maximum - startZ;

            // Number of glyphs in each dimension
            int numGlyphsX = Math.Max(1, (int)(sizeX * glyphDensity));
            int numGlyphsY = Math.Max(1, (int)(sizeY * glyphDensity));
            int numGlyphsZ = Math.Max(1, (int)(sizeZ * glyphDensity));

            // Determine max radius
            float radiusX = (rangeX / (float)numGlyphsX) / 2.0f;
            float radiusY = (rangeY / (float)numGlyphsY) / 2.0f;
            float radiusZ = (rangeZ / (float)numGlyphsZ) / 2.0f;
            float maxRadius = Math.Min(radiusX, Math.Min(radiusY, radiusZ));

            for (int z = 0; z < numGlyphsZ; z++)
            {
                for (int y = 0; y < numGlyphsY; y++)
                {
                    for (int x = 0; x < numGlyphsX; x++)
                    {
                        // grid positions in worldspace
                        float posX = startX + rangeX * (float)(x) / (float)(numGlyphsX) + 0.5f * rangeX / numGlyphsX;
                        float posY = startY + rangeY * (float)(y) / (float)(numGlyphsY) + 0.5f * rangeY / numGlyphsY;
                        float posZ = startZ + rangeZ * (float)(z) / (float)(numGlyphsZ) + 0.5f * rangeZ / numGlyphsZ;

                        // gradient positions in texture space
                        float volPosX = (float)(sizeX * x) / (float)(numGlyphsX);
                        float volPosY = (float)(sizeY * y) / (float)(numGlyphsY);
                        float volPosZ = (float)(sizeZ * z) / (float)(numGlyphsZ);
                        float density = voxels.ElementAt((int)volPosX + (int)volPosY * sizeX + (int)volPosZ * sizeX * sizeY);


                        // Render only if the density is greater than the threshold
                        if (density > glyphThreshold)
                        {
                            // Calculate MVP matrix of glyph
                            Matrix4 model = boundingBox.ModelMatrix;

                            // Get the magnitude of the gradient and map it to [0, 1]
                            Vector3 gradient = new Vector3(gradients2.ElementAt((int)volPosX + (int)volPosY * sizeX + (int)volPosZ * sizeX * sizeY));
                            float magn = gradient.Length;
                            float radius = Math.Max(0.0f, (magn / maxMagn) * maxRadius);

                            // Apply global translation and scale
                            Matrix4 translation = Matrix4.CreateTranslation(posX, posY, posZ);
                            Matrix4 scale = Matrix4.CreateScale(radius);
                            model = scale * translation * model;

                            // Apply local inverse scale on the glyph to get a sphere
                            Matrix4 inverseScale = Matrix4.CreateScale(boundingBox.ModelMatrix.ExtractScale());
                            inverseScale = Matrix4.Invert(inverseScale);
                            model = inverseScale * model;

                            // Apply the VP matrix
                            mvp = model * viewProjection;

                            glypShader.UseShader();

                            GL.BindVertexArray(glyph.Mesh.Vao);

                            GL.UniformMatrix4(GL.GetUniformLocation(glypShader.ProgramHandle, "u_MVP"), false, ref mvp);
                            GL.Uniform1(GL.GetUniformLocation(glypShader.ProgramHandle, "u_density"), density);

                            GL.DrawElements(PrimitiveType.Triangles, glyph.Mesh.Indices.Count,
                                DrawElementsType.UnsignedInt, IntPtr.Zero);

                            GL.BindVertexArray(0);
                        }
                    }
                }
            }

            #endregion
        }

        private void BoundingBoxRendering(ref Matrix4 mvp)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            boundingBoxShader.UseShader();

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.LineWidth(2.0f);

            GL.BindVertexArray(boundingBox.Mesh.Vao);

            GL.UniformMatrix4(GL.GetUniformLocation(colorCube.ProgramHandle, "u_MVP"), false, ref mvp);

            GL.DrawElements(PrimitiveType.Quads, boundingBox.Mesh.Indices.Count,
                DrawElementsType.UnsignedInt, IntPtr.Zero);

            GL.BindVertexArray(0);

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }
    }
}