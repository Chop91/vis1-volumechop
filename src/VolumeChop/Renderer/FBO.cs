﻿

using OpenTK.Graphics.OpenGL4;
using System;
using VolumeChop.Renderer.Geometry;

namespace VolumeChop.Renderer
{
    class FBO
    {
        private int windowWidth;
        private int windowHeight;

        private uint fbo;


        private uint depthBuffer;

        public uint[] ColorTextures { get; private set; }
        private int numberOfColorTextures;

        private Quad quad;

        public FBO(int windowWidth, int windowHeight)
        {
            this.windowWidth = windowWidth;
            this.windowHeight = windowHeight;
            numberOfColorTextures = 1;
            ColorTextures = new uint[numberOfColorTextures];

            quad = new Quad();
        }

        public bool CreateFbo()
        {
            // Generate fbo, depth buffer and color texture
            GL.GenFramebuffers(1, out fbo);
            GL.GenRenderbuffers(1, out depthBuffer);
            GL.GenTextures(numberOfColorTextures, ColorTextures);

            // Bind fbo and depth buffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthBuffer);


            // Generate color textures
            #region generate color texture

            // Define draw buffers
            DrawBuffersEnum[] drawBuffers = new DrawBuffersEnum[numberOfColorTextures];

            // Bind color texture
            for (int i = 0; i < numberOfColorTextures; i++)
            {
                uint colorTexture = ColorTextures[i];

                GL.BindTexture(TextureTarget.Texture2D, colorTexture);

                // Give an empty image to OpenGL ( the last "0" )
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba16f, windowWidth, windowHeight, 0, PixelFormat.Rgba, PixelType.UnsignedShort, IntPtr.Zero);

                // Generate mipmaps
                GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

                // Filtering
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);

                // Clamp texture
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

                // Set render texture as our color attachement #0 + i
                GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, colorTexture, 0);

                // Set the draw buffer
                drawBuffers[i] = DrawBuffersEnum.ColorAttachment0 + i;
            }

            // Set the list of draw buffers
            GL.DrawBuffers(numberOfColorTextures, drawBuffers);
            #endregion

            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, windowWidth, windowHeight);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, depthBuffer);


            // Always check that our framebuffer is ok
            FramebufferErrorCode error = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (error != FramebufferErrorCode.FramebufferComplete)
            {
                Console.Error.WriteLine(error.ToString());
                return false;
            }

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            return true;
        }

        public void BindFbo()
        {
            // Render to framebuffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
            GL.Viewport(0, 0, windowWidth, windowHeight);
        }

        public void UnbindFbo()
        {
            // Render to screen
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, windowWidth, windowHeight);
        }
    }
}