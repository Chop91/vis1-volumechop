﻿
using OpenTK;
using System.Collections.Generic;

namespace VolumeChop.Renderer
{
    class Parameter
    {
        #region volume rendering

        // General
        public DataSet Data { get; set; }
        public bool BoundingBox { get; set; }

        internal enum DataSet
        {
            Lobster,
            BeetleSmall,
            Beetle,
            HeadSmall,
            Head
        };

        // Raycasting
        public float SampleDistance { get; set; }

        public Technique RenderingTechnique { get; set; }

        internal enum Technique
        {
            MIP,
            DVR,
            Average,
            Glyph
        };

        // Shading
        public bool Shading { get; set; }

        public float ShadingIntensity { get; set; }
        public float DiffuseShading { get; set; }
        public float SpecularShading { get; set; }
        public float SpecularExponent { get; set; }
        public float GradientShading { get; set; }
        public float GradientOpacity { get; set; }

        // Transfer Function
        public List<TransferFunctionNode> TransferFunctionNodes { get; set; }

        // Visual Mapping
        public float GlyphDensity { get; set; }
        public float GlyphThreshold { get; set; }

        #endregion


        #region transformation

        // Camera
        public float FieldOfView { get; set; }
        public Vector3 CameraPosition { get; set; }
        public Vector3 CameraDirection { get; set; }

        // Light
        public Vector3 LightPosition { get; set; }


        // Object
        public float Scale { get; set; }

        public bool AutoRotation { get; set; }

        public float RotationSpeed { get; set; }

        public Quaternion Rotation { get; set; }

        public Matrix4 RotationMatrix { get; set; }

        public Vector3 Translation { get; set; }

        #endregion
    }
}
