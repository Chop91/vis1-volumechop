﻿using OpenTK;
using System.Collections.Generic;
using System.Linq;

namespace VolumeChop.Renderer.Geometry
{
    class Cube : Model
    {
        public Cube()
        {
            List<Vector3> positions;
            List<int> indices;

            CreatePositions(out positions);
            CreateIndices(out indices);

            List<Mesh.Vertex> vertices = positions.Select(p => new Mesh.Vertex { position = p }).ToList();
            Mesh = new Mesh(vertices, indices);

            ResetModelMatrix();
        }

        private void CreatePositions(out List<Vector3> positions)
        {
            positions = new List<Vector3>()
            {
                new Vector3(-1.0f, -1.0f, -1.0f),
                new Vector3(1.0f, -1.0f, -1.0f),
                new Vector3(1.0f, 1.0f, -1.0f),
                new Vector3(-1.0f, 1.0f, -1.0f),
                new Vector3(-1.0f, -1.0f, 1.0f),
                new Vector3(1.0f, -1.0f, 1.0f),
                new Vector3(1.0f, 1.0f, 1.0f),
                new Vector3(-1.0f, 1.0f, 1.0f),
            };
        }

        private void CreateIndices(out List<int> indices)
        {
            indices = new List<int>()
            {
                // quad indices
                // back
                0, 3, 2, 1,

                // top
                2, 3, 7, 6,

                // left
                0, 4, 7, 3,

                // right
                1, 2, 6, 5,

                // front
                4, 5, 6, 7,

                // bottom
                0, 1, 5, 4
            };
        }
    }
}