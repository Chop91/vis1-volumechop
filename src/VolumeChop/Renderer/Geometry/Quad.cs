﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace VolumeChop.Renderer.Geometry
{
    class Quad
    {
        private List<Vertex2D> vertices;

        private uint vao;
        private uint vbo;

        public struct Vertex2D
        {
            public Vector2 position;
            public Vector2 uv;
        }

        public Quad()
        {
            vertices = new List<Vertex2D>();
            GenerateQuad();
            CreateVao();
        }

        public uint Vao
        {
            get { return vao; }
        }

        private void CreateVao()
        {
            // Generate vao and vbo
            GL.GenVertexArrays(1, out vao);
            GL.GenBuffers(1, out vbo);

            // Bind vao and vbo
            GL.BindVertexArray(Vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);

            // Vertex positions
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex2D)),
                IntPtr.Zero);

            // Vertex uv-coordinates
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex2D)),
                Marshal.OffsetOf(typeof(Vertex2D), "uv"));

            // Push data of fullscreen quad
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Count * Marshal.SizeOf(typeof(Vertex2D))), vertices.ToArray(), BufferUsageHint.StaticDraw);

            // Unbind vao and vbo
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        private void GenerateQuad()
        {
            Vertex2D vertexUpLeft;
            Vertex2D vertexUpRight;
            Vertex2D vertexDownRight;
            Vertex2D vertexDownLeft;

            vertexUpLeft.position = new Vector2(-1.0f, 1.0f);
            vertexUpRight.position = new Vector2(1.0f, 1.0f);
            vertexDownRight.position = new Vector2(1.0f, -1.0f);
            vertexDownLeft.position = new Vector2(-1.0f, -1.0f);

            vertexUpLeft.uv = new Vector2(0.0f, 1.0f);
            vertexUpRight.uv = new Vector2(1.0f, 1.0f);
            vertexDownRight.uv = new Vector2(1.0f, 0.0f);
            vertexDownLeft.uv = new Vector2(0.0f, 0.0f);

            vertices.Add(vertexUpLeft);
            vertices.Add(vertexDownLeft);
            vertices.Add(vertexUpRight);

            vertices.Add(vertexDownRight);
            vertices.Add(vertexUpRight);
            vertices.Add(vertexDownLeft);
        }
    }
}