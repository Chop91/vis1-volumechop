﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace VolumeChop.Renderer.Geometry
{
    class Mesh
    {
        private int vao;
        private int vbo;
        private int ebo;

        private List<Vertex> vertices;
        private List<int> indices;

        public Mesh(List<Vertex> vertices, List<int> indices)
        {
            this.vertices = vertices;
            this.indices = indices;

            Setup();
        }

        private void Setup()
        {
            // Generate buffers
            GL.GenVertexArrays(1, out vao);
            GL.GenBuffers(1, out vbo);
            GL.GenBuffers(1, out ebo);

            // Bind everything
            GL.BindVertexArray(vao);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData<Vertex>(BufferTarget.ArrayBuffer,
                (IntPtr)(vertices.Count * Marshal.SizeOf(typeof(Vertex))), vertices.ToArray(),
                BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
            GL.BufferData<int>(BufferTarget.ElementArrayBuffer, (IntPtr)(indices.Count * sizeof(int)),
                indices.ToArray(), BufferUsageHint.StaticDraw);

            // Vertex positions
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                IntPtr.Zero);

            // Vertex normals
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                Marshal.OffsetOf(typeof(Vertex), "normal"));

            // Vertex uv-coordinates
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                Marshal.OffsetOf(typeof(Vertex), "uv"));

            // Vertex color
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)),
                Marshal.OffsetOf(typeof(Vertex), "color"));

            // Unbind everything
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public struct Vertex
        {
            public Vector3 position;
            public Vector3 normal;
            public Vector2 uv;
            public Vector3 color;
        }

        public int Vao
        {
            get { return vao; }
        }

        public List<int> Indices
        {
            get { return indices; }
        }
    }
}