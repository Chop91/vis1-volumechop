﻿using OpenTK;

namespace VolumeChop.Renderer.Geometry
{
    class Model
    {
        private Mesh mesh;
        private Matrix4 modelMatrix;

        public void Rotate(float angle, Vector3 axis)
        {
            Matrix4 fullModelMatrix = ModelMatrix;

            Matrix4 inverseModelMatrix;
            Matrix4.Invert(ref fullModelMatrix, out inverseModelMatrix);

            Matrix4 rotationMatrix;
            Matrix4.CreateFromAxisAngle(axis, angle, out rotationMatrix);


            ModelMatrix *= inverseModelMatrix * rotationMatrix * fullModelMatrix;
        }

        public void Rotate(Matrix4 rotationMatrix)
        {
            Matrix4 fullModelMatrix = ModelMatrix;

            Matrix4 inverseModelMatrix;
            Matrix4.Invert(ref fullModelMatrix, out inverseModelMatrix);

            ModelMatrix *= inverseModelMatrix * rotationMatrix * fullModelMatrix;
        }

        public void Scale(Vector3 scaleVector)
        {
            Matrix4 fullModelMatrix = ModelMatrix;

            Matrix4 inverseModelMatrix;
            Matrix4.Invert(ref fullModelMatrix, out inverseModelMatrix);

            Matrix4 scaleMatrix;
            Matrix4.CreateScale(ref scaleVector, out scaleMatrix);

            ModelMatrix *= inverseModelMatrix * scaleMatrix * fullModelMatrix;
        }

        public void Translate(Vector3 translationVector)
        {
            Matrix4 fullModelMatrix = ModelMatrix;

            Matrix4 inverseModelMatrix;
            Matrix4.Invert(ref fullModelMatrix, out inverseModelMatrix);

            Matrix4 translationMatrix;
            Matrix4.CreateTranslation(ref translationVector, out translationMatrix);


            ModelMatrix *= inverseModelMatrix * translationMatrix * fullModelMatrix;
        }

        public void ResetModelMatrix()
        {
            ModelMatrix = Matrix4.Identity;
            //Rotate((float)(Math.PI / 2.0f), new Vector3(1, 0, 0));
            //Rotate((float)(Math.PI / 2.0f), new Vector3(0, 0, 1));
        }

        public Mesh Mesh
        {
            get { return mesh; }
            protected set { mesh = value; }
        }

        public Matrix4 ModelMatrix
        {
            get { return modelMatrix; }
            protected set { modelMatrix = value; }
        }
    }
}