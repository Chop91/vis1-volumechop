#version 400 core

layout(location = 0) out vec4 o_color;

in vec2 f_uv;

uniform sampler2D u_texture_front;
uniform sampler2D u_texture_back;
uniform sampler3D u_texture_volume;

uniform float u_unit_step;
uniform float u_sample_distance;

void main()
{
	vec3 frontface = texture(u_texture_front, f_uv).xyz;
	vec3 backface = texture(u_texture_back, f_uv).xyz;
	vec3 view_direction = normalize(backface - frontface);

	float step_size = u_unit_step * u_sample_distance;
	vec3 ray_increment = view_direction * step_size;
	float accumulated_sample_value = 0.0;
	int sample_count = 0;
	
	for(float ray_length = length(backface - frontface); ray_length > 0.0; ray_length = ray_length - step_size)
	{
		// Get the sample value (GL_LUMINANCE: r = g = b; a = 1.0)
		float sample_value = (texture(u_texture_volume, frontface)).g;

		// Accumulate the sample values
		if(sample_value > 0.0)
		{
			accumulated_sample_value += sample_value;
			sample_count++;
		}

		// Calculate the next position
		frontface += ray_increment;
	}
	
	// Calculate the average sample value
	float avg_sample_value = accumulated_sample_value / sample_count;

	o_color = vec4(avg_sample_value, avg_sample_value, avg_sample_value, 1.0);
}