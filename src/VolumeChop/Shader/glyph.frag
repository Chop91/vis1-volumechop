#version 400 core

layout(location = 0) out vec4 o_color;

in vec4 f_color;

uniform float u_density;

void main()
{
	vec4 red = vec4(1, 0, 0, 1);
	vec4 green = vec4(0, 1, 0, 1);
	
	o_color = mix(green, red, u_density);
}