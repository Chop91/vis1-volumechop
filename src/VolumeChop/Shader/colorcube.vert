#version 400 core

layout(location = 1) in vec3 v_position;

out vec4 f_color;

uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * vec4(v_position, 1.0);

	float x = clamp(v_position.x, 0.0, 1.0);
	float y = clamp(v_position.y, 0.0, 1.0);
	float z = clamp(v_position.z, 0.0, 1.0);
	
	f_color = vec4(x, y, z, 1.0);
}