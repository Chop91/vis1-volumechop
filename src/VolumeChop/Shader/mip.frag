#version 400 core

layout(location = 0) out vec4 o_color;

in vec2 f_uv;

uniform sampler2D u_texture_front;
uniform sampler2D u_texture_back;
uniform sampler3D u_texture_volume;

uniform float u_unit_step;
uniform float u_sample_distance;

void main()
{
	vec3 frontface = texture(u_texture_front, f_uv).xyz;
	vec3 backface = texture(u_texture_back, f_uv).xyz;
	vec3 view_direction = normalize(backface - frontface);

	float step_size = u_unit_step * u_sample_distance;
	vec3 ray_increment = view_direction * step_size;
	float max_sample_value = 0.0;
	
	for(float ray_length = length(backface - frontface); ray_length > 0.0; ray_length = ray_length - step_size)
	{
		// Get the sample value (GL_LUMINANCE: r = g = b; a = 1.0)
		float sample_value = (texture(u_texture_volume, frontface)).g;

		// Get the current maximum
		max_sample_value = max(sample_value, max_sample_value);

		// Calculate the next position
		frontface += ray_increment;
	}
	
	o_color = vec4(max_sample_value, max_sample_value, max_sample_value, 1.0);
}