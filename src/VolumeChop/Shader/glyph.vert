#version 400 core

layout(location = 1) in vec3 v_position;

out vec4 f_color;

uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * vec4(v_position, 1.0);
	
	float r = 1.0;
	float g = 0.0;

	f_color = vec4(r, g, 0.0, 1.0);
}