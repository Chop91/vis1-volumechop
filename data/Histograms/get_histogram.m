close all; clear;

path = '..\CSV\';
files = dir([path '*.csv']);

for i = 1:size(files, 1)
     data = csvread([path files(i).name]);
     data(data==0) = []; %remove zeros - no volume data
     figure;
     hist(data, sqrt(2.^12));
     
     filename = files(i).name;
     filename = strrep(filename,'.csv','');
     title(filename, 'Interpreter', 'none');
     xlabel('Density');
     ylabel('Frequency');
     xlim([0 1]);
end