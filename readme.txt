Visualisierung 1 - Rendern von Volumsdaten

Implementation:
- Raycasting
- GPU-Implementierung
- Alpha-Compositing
- Berechnung der Gradienten
- Gradientenbasiertes Shading
- Interaktionsmöglichkeiten
- Weitere Renderingtechnik


Projektstruktur:
- data
	CSV, Histogramme und Volumsdaten
- doc
	verwendete Literatur
- src
	C# Visual Studio 2013 Projekt
- submission
	ausführbarer Volumerenderer(Release-Version)